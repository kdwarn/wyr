# What You've Read

No longer in operation.

[What You've Read](https://www.whatyouveread.com) is a service that allows users to store links to and basic metadata on articles, blog posts, books, reference items, webpages, podcasts, movies, and any other sort of content that can be read, listened to, or watched. A bookmarklet dragged to a browser toolbar makes it easy to save webpages to WYR, and WYR can import bookmarks as well as the data for documents in Mendeley and books on Goodreads, creating a space where all of these items can be viewed at once and filtered by tags, authors, and read status.
